import hdfs, pdb, os, yaml, json

def walk_fs(path, url="http://$HADOOP_URL:9999"):
    cli = hdfs.Client(url)
    data_dict = {}
    for dirname, dirnames, filenames in cli.walk(path):
        dn = os.path.basename(dirname)
        data_dict[dn] = []
        #for f in files:
        if dirnames:
            for d in dirnames:
                data_dict[dn].append(get_fs2yaml(path=os.path.join(path, d)))
            for f in filenames:
                data_dict[dn].append(f)
        else:
            data_dict[dn] = filenames
        return data_dict

def filestructure2yaml(path):
    data = walk_fs(path)
    #print(data)
    try:
        with open("/tmp/file_structure.yaml", "w") as yf:
            try:
                yaml.dump(data, yf, default_flow_style=False)
            except Exception as e:
                print(e)
    except Exception as e:
        print(e)

def list_fs(path, url="http://$HADOOP_URL:9999"):
    cli = hdfs.Client(url, timeout=100, session=False)
    #print(cli.list(path))
    output_dict = {}
    output_dict["current_path"] = path
    output_dict["file"] = []
    output_dict["directory"] = []
    data_info_list = []
    for f in cli.list(path):
        target = os.path.join(path, f)
        target_info = cli.status(target)
        target_type = target_info["type"].swapcase()
        data_info_list.append(target_info)
        output_dict[target_type].append(f)
    return output_dict
    

if __name__ == "__main__":
    data = list_fs("/prmon_harvester")
    print(json.dumps(data, indent=4))
