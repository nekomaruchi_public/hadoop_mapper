import os
from subprocess import PIPE, Popen
import shlex
import json

import parameter_checker
import datetime




@parameter_checker.checker
def hadoop_commander(args, output=None):

    parser = args
    HDFS_input = parser.input 
    output_path = output
    keyword = parser.keyword
    mapper = parser.mapper 
    reducer = parser.reducer
    display = parser.display
    mapper_file = mapper.split(" ", 1)[0]
    reducer_file = reducer.split(" ", 1)[0]
    args = ["hadoop",
            "jar",
            "/opt/hadoop-3.1.2/share/hadoop/tools/lib/hadoop-*stream*.jar", 
            "-file", mapper_file,
            "-file", reducer_file,
            "-mapper", '\"{0}\"'.format(mapper),
            "-reducer", "\"{0}\"".format(reducer),
            "-input", HDFS_input,
            "-output", "\"{0}\"".format(output)
            ]


    if not display:
        p = Popen(" ".join(args), stdout=PIPE, stderr=PIPE, shell=True, encoding="utf-8")
        stdout, stderr = p.communicate()
        log_file = os.path.join(output, "hadoop_output.log")
        with open(log_file, "a") as f:
            date_string = datetime.datetime.now()
            date_string = str(date_string)[:19]
            split_banner = "{}=============================================================".format(date_string)
            f.write("\n"+split_banner+"\n")
            f.write(stdout)
            f.write("\n=============================================================\n")
            f.write(stderr)
        
        
    elif display:
        line = _run_command(" ".join(args)) 
        
        log_file = os.path.join(output, "hadoop_output.log")

        with open(log_file, "a") as f:
            date_string = datetime.datetime.now()
            date_string = str(date_string)[:19]
            split_banner = "{}=============================================================".format(date_string)
            f.write("\n"+split_banner+"\n")
            f.write("\n".join(line))

    
def search_word(costom_path, words, must_match=False):
    PATH = os.path.split(os.path.realpath(__file__))[0]
    search_file = {"must_match": f'{must_match}', 
                   "words": words}

    with open('{}/search_file'.format(PATH), 'w') as f:
        f.write( json.dumps(search_file) )
  
def _run_command(command):
    p = Popen(command, shell=True, stdout=PIPE)
    lines=[]
    for line in iter(p.stdout.readline, b''):
        line = line.strip().decode("utf-8")
        print(line)
        lines.append(line)
    return lines
