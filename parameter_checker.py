import os
from functools import wraps

PATH = os.path.split(os.path.realpath(__file__))[0]

class MyException(Exception):
    pass

class IfNotFile(Exception):
    def __init__(self, filename):
        self.filename = filename
    def __str__(self):
        return f'Cannot find file named {self.filename}.'
    pass

def exception_handler(function):
    @wraps(function)
    def funct(*args, **kwargs):
        try: 
            print(args) 
            return function(*args, **kwargs)
        except IfNotFile :
            print("error")
            return -1

    return funct


#@exception_handler
def checker(func):
    def wrapper(*args, **kwargs):
        #pwd = os.getcwd()
        argument = args[0]
        
        if not os.path.isfile(argument.mapper.split(" ", 1)[0]):
            raise IfNotFile(argument.mapper.split(" ", 1)[0])
        if not os.path.isfile(argument.reducer.split(" ", 1)[0]):
            raise IfNotFile(argument.reducer.split(" ", 1)[0])
        if len(argument.mapper) ==0:
            argument.mapper=[""]

        return func(*args, **kwargs)
    return wrapper



def _isBlank (myString):
    return not (myString and myString.strip())

