#!/bin/bash

hdfs_URL="hdfs://$HADOOP_IP:9999"
data_DIR="sflow"
data="20200922.tar.gz"
inputtarget="${hdfs_URL}/${data_DIR}/${data}"

output_DIR="/tmp"
output="sflow_mapR_$(date +%s)"
outputtarget="${output_DIR}/${output}"

mapperfile="`pwd`/sflow_mapper.py"
reducerfile="`pwd`/sflow_reducer.py"

CMD="hadoop jar /opt/hadoop-3.1.2/share/hadoop/tools/lib/hadoop-*stream*.jar -file ${mapperfile} -mapper ${mapperfile} -file ${reducerfile} -reducer ${reducerfile} -input ${inputtarget} -output ${outputtarget}"

echo "Running : ${CMD}"
eval ${CMD}
