#!/usr/bin/env python

import sys, json, re
""" mapper """

col22 = ["agent","srcIP","dstIP","IPProtocol","TCPSrcPort","TCPDstPort","summedPacketSize","flow_stime","flow_etime","flow_pktcnt","packetSequenceNo","sampleSequenceNo","inputPort","outputPort","srcMAC","dstMAC","in_vlan","out_vlan","nextHop","src_as","dst_as","IPTTL"]
col20 = ['agent', 'unixSecondsUTC', 'packetSequenceNo','sampleSequenceNo', 'inputPort', 'outputPort', 'srcMAC', 'dstMAC', 'srcIP', 'dstIP', 'IPProtocol', 'TCPSrcPort', 'TCPDstPort', 'in_vlan', 'out_vlan', 'nextHop', 'src_as', 'dst_as', 'sampledPacketSize', 'IPTTL']

column_dict = {
    22 : col22, 20 : col20
}

def to_dict(data):
    #data = input.split(',')
    data = dict(
        zip(column_dict[len(data)], data)
    )
    print(data)

if __name__ == "__main__":
    for line in sys.stdin:
        line = line.strip()
        if len(line) > 2 :
            if "," in line:
                data = line.split(",")
            elif ":" in line:
                data = line.split(":")
            if (len(data)==22 or len(data)==20) and ("srcIP" not in data): pass

            elif "srcIP" in data:
                tmp_data = data[0].replace("\\x00", "")
                to_dict([tmp_data] + data[1:])
                
            else: pass
        else: pass
