#!/usr/bin/env python

import sys, json, re
""" mapper """

col22 = ["agent","srcIP","dstIP","IPProtocol","TCPSrcPort","TCPDstPort","summedPacketSize","flow_stime","flow_etime","flow_pktcnt","packetSequenceNo","sampleSequenceNo","inputPort","outputPort","srcMAC","dstMAC","in_vlan","out_vlan","nextHop","src_as","dst_as","IPTTL"]
col20 = ['agent', 'unixSecondsUTC', 'packetSequenceNo','sampleSequenceNo', 'inputPort', 'outputPort', 'srcMAC', 'dstMAC', 'srcIP', 'dstIP', 'IPProtocol', 'TCPSrcPort', 'TCPDstPort', 'in_vlan', 'out_vlan', 'nextHop', 'src_as', 'dst_as', 'sampledPacketSize', 'IPTTL']

column_dict = {
    22 : col22, 20 : col20
}

def to_dict(data):
    #data = input.split(',')
    data = dict(
        zip(column_dict[len(data)], data)
    )
    print(data)

if __name__ == "__main__":
        
    for line in sys.stdin.readlines():
        line = line.strip()
        if len(line) > 2 :
            if "," in line:
                data = line.split(",")
            elif ":" in line:
                data = line.split(":")
            if (len(data)==22 or len(data)==20) and ("srcIP" not in data):
                if "202.169.169.14" in data: pass
                    #to_dict(data)
            elif "srcIP" in data: 
                #data = [x.replace("\\x00", "") for x in data if "srcIP" in x]
                #data = line.replace("\\x00", "")
                print(data[0])
                #to_dict(data)
        else: pass
