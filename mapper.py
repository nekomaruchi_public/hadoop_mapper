#!/usr/bin/env python

import sys, json, re
import json
import os

""" mapper """

#col22 = ["agent","srcIP","dstIP","IPProtocol","TCPSrcPort","TCPDstPort","summedPacketSize","flow_stime","flow_etime","flow_pktcnt","packetSequenceNo","sampleSequenceNo","inputPort","outputPort","srcMAC","dstMAC","in_vlan","out_vlan","nextHop","src_as","dst_as","IPTTL"]
#col20 = ['agent', 'unixSecondsUTC', 'packetSequenceNo','sampleSequenceNo', 'inputPort', 'outputPort', 'srcMAC', 'dstMAC', 'srcIP', 'dstIP', 'IPProtocol', 'TCPSrcPort', 'TCPDstPort', 'in_vlan', 'out_vlan', 'nextHop', 'src_as', 'dst_as', 'sampledPacketSize', 'IPTTL']

#column_dict = {
#    22 : col22, 20 : col20
#}

def to_dict(data):
    #data = input.split(',')
    data = dict(
        zip(column_dict[len(data)], data)
    )

class search_file:
    def __init__(self):
        PATH = os.path.split(os.path.realpath(__file__))[0]
        search_data = ""
        with open(f'{PATH}/search_file', 'r' ) as f:
            search_data = json.load(f)
        self.must_match = search_data.get("must_match", False)
        self.keywords = search_data.get("words", False)

    def _findword_with_OR(self):
        w = "|".join(self.keywords)
        #print (w) 
        return re.compile(r'{0}'.format(w), flags=re.IGNORECASE).search
    
    def _findword_with_AND(self):
        rex=""
        for string in self.keywords:
            rex = rex + r'(?=.*{0})'.format(string)
         
        return re.compile(r'{0}.*'.format(rex), flags=re.IGNORECASE).search

    def search(self):
        
        if self.must_match == "True":
            return self._findword_with_AND()
        else:
            return self._findword_with_OR()

    

#def read_searchfile():
#    PATH = os.path.split(os.path.realpath(__file__))[0] 
#    search_data=""
#    with open(f'{PATH}/search_file', 'r' ) as f:
#        search_data = json.load(f)    
#    search_data.get("must_match", False)

if __name__ == "__main__":        

    search_dict = search_file()

    for line in sys.stdin.readlines():
        line = line.strip()
        #print(line)
            #if "," in line:
            #    data = line.split(",")
            #elif ":" in line:
            #    data = line.split(":")
            #if (len(data)==22 or len(data)==20) and ("srcIP" not in data):
            #    if "202.169.169.14" in data: pass
                    #to_dict(data)
            #if "srcIP" in data:
        if search_dict.search()(line):
            #data = [x.replace("\\x00", "") for x in data if "srcIP" in x]
            #data = line.replace("\\x00", "")
            #print(data[0])
            print(line)
            #to_dict(data)
