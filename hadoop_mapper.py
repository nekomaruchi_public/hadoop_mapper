import os
import argparse
import time 

import core


PATH = os.path.split(os.path.realpath(__file__))[0]

DEF_MAP= os.path.join(PATH, "mapper.py")
DEF_RED= os.path.join(PATH, "reducer.py")
OUTPUT= os.path.join(PATH, "output_{}/".format(int(time.time())))


def main():
    parser = argparse.ArgumentParser(add_help=True)

    parser.add_argument("-ma", "--match", dest="match", action="store_true", default=False, help="Math all the words for filter.")
    parser.add_argument("-m", "--mapper", dest="mapper", action="store", default=DEF_MAP, help="Specify mapper file for filter the file(s).")
    parser.add_argument("-r", "--reducer", dest="reducer", action="store", default=DEF_RED, help="Specify reduce file for transform text form.")
    parser.add_argument("-i", "--input", dest="input", required=True, action="store", help="Specify the path for get target file(s).")
    parser.add_argument("-display", dest="display", default=False, action="store_true", help="Show the output when run hadoop, the speed may slower than usual.")
    #parser.add_argument("-o", "--output", dest="output", required=True, action="store", help="Specify the path to put output.")
    parser.add_argument("keyword", nargs='*', type=str, action="store", metavar="<words>", help="Words for search.")


    args = parser.parse_args() 
    #print(args)
    core.search_word(args.input, args.keyword, must_match=args.match)
    core.hadoop_commander(args, output=OUTPUT)

if __name__ == "__main__":

    try:
        main()
    except KeyboardInterrupt:
        sys.exit(FAILURE)
